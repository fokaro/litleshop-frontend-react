import { useState, useEffect } from "react";
import Header from "./Header";
import { Link } from "react-router-dom";
import Footer from "./Footer";

export default function Marchandise() {
  const [id, setId] = useState("");
  const [name, setName] = useState("");
  const [unit_price, setUnit_price] = useState("");
  const [total_price, setTotal_price] = useState("");
  const [quantity, setQuantity] = useState("");
  const [marchandise, setMarchandise] = useState([]);
  const [detailsMarchandise, setDetailsMarchandise] = useState("");

  const changeName = (e) => {
    let Name = e.target.value;
    console.log(Name);
    setName(Name);
  };

  const changeUnit_price = (e) => {
    let unit_price = e.target.value;
    console.log(unit_price);
    setUnit_price(unit_price);
  };

  const changeTotal_price = (e) => {
    let total_price = e.target.value;
    console.log(total_price);
    setTotal_price(total_price);
  };

  const changeQuantity = (e) => {
    let Quantity = e.target.value;
    console.log(Quantity);
    setQuantity(Quantity);
  };

  const deleteMarchandise = async (e) => {
    const id = e.target.value;
    let options = {
      method: "POST",
      headers: { "Content-type": "application/json;charset=utf-8" },
      body: JSON.stringify({ id: id }),
    };
    await fetch("http://localhost:8000/api/delete-marchandise", options).then(
      (response) => response.json()
    );
    console.log('me voici');
     updateData();
  };

  const details = async (e) => {
    const id = e.target.value;
    let options = {
      method: "POST",
      headers: { "Content-type": "application/json;charset=utf-8" },
      body: JSON.stringify({ marchandise_id: id }),
    };
    const data = await fetch(
      "http://localhost:8000/api/get-one-marchandise",
      options
    ).then((response) => response.json());
    await updateData();
    setDetailsMarchandise(data);
  };

  const updateMarchandise = async (e) => {
    const id = e.target.value;
    let options = {
      method: "POST",
      headers: { "Content-type": "application/json;charset=utf-8" },
      body: JSON.stringify({ id: id }),
    };
    const data = await fetch(
      "http://localhost:8000/api/get-one-marchandise",
      options
    ).then((response) => response.json());
    console.log(data);
    setName(data.name);
    setQuantity(data.quantity);
    setTotal_price(data.total_price);
    console.log(total_price);
    setUnit_price(data.unit_price);
    setId(data.id);
   
  };

  const updateData = async () => {
    // You can await here
    const data = await fetch("http://localhost:8000/api/get-marchandise").then(
      (response) => response.json()
    );

    setMarchandise(data);
  };

  useEffect(() => {
    async function fetchData() {
      // You can await here
      const data = await fetch(
        "http://localhost:8000/api/get-marchandise"
      ).then((response) => response.json());

     // console.log(data);
      setMarchandise(data);
    }
    fetchData();
  }, []);

  const createMarchandise = async () => {
    const data = {
      name: name,
      quantity: quantity,
      unit_price: unit_price,
      total_price: total_price,
    };
    console.log(data);
    let options = {
      method: "POST",
      headers: { "Content-type": "application/json;charset=utf-8" },
      body: JSON.stringify(data),
    };
    await fetch("http://localhost:8000/api/create", options).then((response) =>
      response.json()
    );
    await updateData();
  };

  const changeMarchandise = async () => {
    const data = {
      id: id,
      name: name,
      quantity: quantity,
      unit_price: unit_price,
      total_price: total_price,
    };
    console.log(data);
    let options = {
      method: "POST",
      headers: { "Content-type": "application/json;charset=utf-8" },
      body: JSON.stringify(data),
    };
    await fetch("http://localhost:8000/api/update-marchandise", options).then(
      (response) => response.json()
    );
    await updateData();
  };

  return (
    <>
          <Header />
          <table className="table" border="3px" >
            <thead class="thead-dark">
            <tr>
              <th>name</th>
              <th>quantite</th>
              <th>unit_price</th>
              <th>total_price</th>
              <th>delete</th>
              <th>detail</th>
              <th>edit</th>
            </tr>
            </thead>
            {marchandise?.map((item) => (
              <tr key={item.id} class="table-primary">
                <td>{item.name}</td>
                <td>{item.quantity}</td>
                <td>{item.unitPrice}</td>
                <td>{item.totalPrice}</td>
                <td>
                  <button
                    type="button"
                    class="btn btn-danger mb-2"
                    onClick={deleteMarchandise}
                    value={item.id}
                  >
                    delete
                  </button>
                </td>
                <td>
                    <Link to={{pathname:"/details",search:"?marchandiseId="+item.id}}>
                  <button type="button"
                  class="btn btn-info mb-2" 
                 /*  onClick={details}  */
                 
                  value={item.id}>
                    details
                  </button>
                  </Link>
                </td>
                <td>
                <Link to={{pathname:"/editer",search:"?marchandiseId="+item.id}}>

                  <button
                    type="button"
                    class="btn btn-success mb-2" 

/*                     onClick={updateMarchandise}
 */                    value={item.id}>
                    editer
                  </button>
                  </Link>
                </td>
              </tr>
            ))}
          </table>
        
      <Footer/>
    </>
  );
}
