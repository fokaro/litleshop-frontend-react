import { useState, useEffect } from "react";
import { useSearchParams } from "react-router-dom";
import Header from "./Header";
import Footer from "./Footer";
import "./style1.css";

export default function MarchandiseDetails() {
  const [marchandise, setMarchandise] = useState("");
  const [searchParams]= useSearchParams();
  const [loader, setLoader] = useState("true");


  useEffect(() => {
    async function fetchData() {
      const marchandiseId=searchParams.get('marchandiseId')
      // You can await here
      let options = {
        method: "POST",
        headers: { "Content-type": "application/json;charset=utf-8" },
        body: JSON.stringify({ id: marchandiseId }),
      };
      const data = await fetch(
        "http://localhost:8000/api/get-one-marchandise",
        options
      ).then((response) => response.json());
      if (data) {setLoader(false);
  
}
      console.log(searchParams);
      console.log(data);
      setMarchandise(data);
      console.log(data.totalPrice)
    }
    fetchData();
  }, [searchParams]);

  return (
    <>
      <Header />
      
   { loader ?  (
   
      <div class="loader">
        </div>) : 
       (<>
    
    <div class="row">
    <div  class="col-md-3"></div>
    <div class="col-md-6">
        <div class="text-center">
             <div class="p-2 mb-2 bg-success text-white">
               <h1 class="display-2">Formulaire</h1>
             </div>
           </div>
           <br></br>
            {/* <div className="col-md-6"> */}
             <div>
                 <div class="container text-center">
                   <h2 class="text-center">
                     <div class="text-success">DETAIL Marchandise</div>
                   </h2>
                   <label>
                    
                     <div class="text-primary">Name= </div>
                     
                   </label>
                   <label> {marchandise.name}</label>
        
                   <div>
                     <label>
                       <div class="text-primary">Quantity=</div>
                     </label>
                     <label> {marchandise.quantity}</label>
                   </div>
                   <div>
                     <label>
                       <div class="text-primary">Unit_price=</div>
                     </label>
                     <label> {marchandise.unitPrice}</label>
                   </div>
                   <div>
                     <label>
                       <div class="text-primary">Total_price=</div>
                     </label>
                     <label> {marchandise.totalPrice}</label>
                   </div>
                   </div>
                 </div>
               </div>
             </div>
          </>)   
        }
      <br></br>
      <Footer/>

    </>
  );
}



