import { useState, useEffect } from "react";
import { useSearchParams } from "react-router-dom";
import Footer from "./Footer";
import Header from "./Header";
import "./styleEdit.css";

export default function MarchandiseEditer() {
  const [searchParams] = useSearchParams();
  const [id, setId] = useState("");
  const [name, setName] = useState("");
  const [unitPrice, setUnitPrice] = useState("");
  const [totalPrice, setTotalPrice] = useState("");
  const [quantity, setQuantity] = useState("");
  const [marchandise, setMarchandise] = useState([]);
  const [isOpen, setIsOpen] = useState(true);

  const changeName = (e) => {
    let Name = e.target.value;
    console.log(Name);
    setName(Name);
  };

  const changeUnitPrice = (e) => {
    let unitPrice = e.target.value;
    console.log(unitPrice);
    setUnitPrice(unitPrice);
  };

  const changeTotalPrice = (e) => {
    let totalPrice = e.target.value;
    console.log(totalPrice);
    setTotalPrice(totalPrice);
  };

  const changeQuantity = (e) => {
    let Quantity = e.target.value;
    console.log(Quantity);
    setQuantity(Quantity);
  };

  useEffect(() => {
    async function fetchData() {
      const marchandiseId = searchParams.get("marchandiseId");
      let options = {
        method: "POST",
        headers: { "Content-type": "application/json;charset=utf-8" },
        body: JSON.stringify({ id: marchandiseId }),
      };
      const data = await fetch(
        "http://localhost:8000/api/get-one-marchandise",
        options
      ).then((response) => response.json());
      setName(data.name);
      setQuantity(data.quantity);
      setTotalPrice(data.totalPrice);
      setUnitPrice(data.unitPrice);
      setId(data.id);
      console.log(totalPrice);
    }
    fetchData();
  }, [searchParams]);

  const updateData = async () => {
    // You can await here
    const data = await fetch("http://localhost:8000/api/get-marchandise").then(
      (response) => response.json()
    );

    setMarchandise(data);
  };

  const changeMarchandise = async () => {
    const data = {
      id: id,
      name: name,
      quantity: quantity,
      unitPrice: unitPrice,
      totalPrice: totalPrice,
    };
    console.log(data);
    let options = {
      method: "POST",
      headers: { "Content-type": "application/json;charset=utf-8" },
      body: JSON.stringify(data),
    };
    const valeur = await fetch(
      "http://localhost:8000/api/update-marchandise",
      options
    ).then((response) => response.json());
    if (valeur != null) {
      setIsOpen(false);

      console.log("voici le nouvel objet");
      console.log(valeur);
    }
console.log(totalPrice);
    await updateData();
  };
  

  return (
    <>
      <Header />
      {isOpen ? (
        <form class="contenu">
          <h2 className="btn btn-info">Formulaire d'edition</h2>
          <br></br>
          <div>
            <div class="row mb-3">
              <label For="Name" class="col-sm-1 col-form-label">
                Name
              </label>
              <div class="col-sm-6">
                <input
                  readonly
                  class="form-control"
                  type="text"
                  id="name"
                  value={name}
                  onChange={changeName}
                />
              </div>
            </div>
          </div>

          <div class=" mb-3 row">
            <label For="Quantity" class="col-sm-1 col-form-label">
              Quantity
            </label>
            <div class="col-sm-6">
              <input
                readonly
                class="form-control"
                type="number"
                id="quantity"
                value={quantity}
                onChange={changeQuantity}
              />
            </div>
          </div>

          <div class=" mb-3 row ">
            <label For="Unit_price" class="col-sm-1 col-form-label">
              Unit_price
            </label>
            <div class="col-sm-6">
              <input
                readonly
                class="form-control"
                id="unit_price"
                type="number"
                value={unitPrice}
                onChange={changeUnitPrice}
              />
            </div>
          </div>
          <div class="row mb-3">
            <label For="Total_price" class="col-sm-1 col-form-label">
              Total_price
            </label>
            <div class="col-sm-6">
              <input
                readonly
                class="form-control"
                id="total_price"
                type="number"
                value={totalPrice}
                onChange={changeTotalPrice}
              />
            </div>
          </div>
          <br></br>
          <div>
            <button
              class="btn btn-info"
              type="button"
              onClick={changeMarchandise}
            >
              Cliquez ici
            </button>
          </div>

          <br></br>
          <div></div>
        </form>
      ) : (
        <div class="alert alert-success" role="alert">
          A simple success alert—check it out!
        </div>
      )}
      <div class="page">
        <h2>
          <a href="Marchandise">Retournez a la page d'accueil</a>
        </h2>
      </div>

      <Footer />
    </>
  );
}
